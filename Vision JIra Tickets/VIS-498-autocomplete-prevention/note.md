### Fixing autocomplete for passwords 

#### As we set passwords for recordings, shortlists and other places browsers will try to autocomplete which we want to prevent.

#### Need your guys opinions on what would be the most appropriate method to use in our use case

#### What we currently do
- We do use `autocomplete='off'` as an `input` attribute but it doesn't work on all browsers, not even chrome properly 
- Edits to fix could be applied in more locations
  - Set room password 
  - Set shortlist password
```html
<div id="fakedeets" style="display: block; position:absolute; bottom:-110px;">
    <input style="display:block; border: 0px solid white;" type="text" name="fakeusernameremembered"/>
    <input style="display:block; border: 0px solid white;" type="password" name="fakepasswordremembered"/>
</div>
```
- This is only being used in the edit room recording page
#### Suggested fixes:

- Keeping type as `'text'` and on input to have it change to `'password'` 
```html
<input type="text" id="password" name="password" oninput="this.type='password'">
```
- Have the field as read only, and on focus remove `'readonly'` attribute
```html
<input type="password" id="password" name="password" readonly="readonly" onfocus="this.removeAttribute('readonly')">
```
- The same as a above but a `setTimeout` is run instead from a `<script>` tag or the js file 
  - This will a lot of extra codes 
```html
<input type="password" id="password" name="password" readonly="readonly">
<script>
  setTimeout(document.getElementById("password").removeAttribute("readonly"), 100)
</script>
```
- A jQuery plugin which randomizes the `type` and changes back on the submit click
  - [jQuery Disable Auto Fill](https://github.com/terrylinooo/jquery.disableAutoFill)
  - It displays disgusting asterisks like `*****` instead of the nice round black circles 
  - Need to include in every page  